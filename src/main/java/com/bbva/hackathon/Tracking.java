package com.bbva.hackathon;

import java.sql.Time;
import java.util.Date;

public class Tracking {

    private String nombre_estado;

    public String getNombre_estado() {
        return nombre_estado;
    }

    public void setNombre_estado(String nombre_estado) {
        this.nombre_estado = nombre_estado;
    }

    public String getDescricion_estado() {
        return descricion_estado;
    }

    public void setDescricion_estado(String descricion_estado) {
        this.descricion_estado = descricion_estado;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public String getCod_operacion() {
        return cod_operacion;
    }

    public void setCod_operacion(String cod_operacion) {
        this.cod_operacion = cod_operacion;
    }

    public String getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(String id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getFecha_estado_ini() {
        return fecha_estado_ini;
    }

    public void setFecha_estado_ini(String fecha_estado_ini) {
        this.fecha_estado_ini = fecha_estado_ini;
    }

    public String getHora_estado_ini() {
        return hora_estado_ini;
    }

    public void setHora_estado_ini(String hora_estado_ini) {
        this.hora_estado_ini = hora_estado_ini;
    }

    public String getFecha_estado_fin() {
        return fecha_estado_fin;
    }

    public void setFecha_estado_fin(String fecha_estado_fin) {
        this.fecha_estado_fin = fecha_estado_fin;
    }

    public String getHora_estado_fin() {
        return hora_estado_fin;
    }

    public void setHora_estado_fin(String hora_estado_fin) {
        this.hora_estado_fin = hora_estado_fin;
    }

    private String descricion_estado;
    private String comentario;
    private String cod_operacion;
    private String id_cliente;
    private String fecha_estado_ini;

    @Override
    public String toString() {
        return "Tracking{" +
                "nombre_estado='" + nombre_estado + '\'' +
                ", descricion_estado='" + descricion_estado + '\'' +
                ", comentario='" + comentario + '\'' +
                ", cod_operacion='" + cod_operacion + '\'' +
                ", id_cliente='" + id_cliente + '\'' +
                ", fecha_estado_ini='" + fecha_estado_ini + '\'' +
                ", hora_estado_ini='" + hora_estado_ini + '\'' +
                ", fecha_estado_fin='" + fecha_estado_fin + '\'' +
                ", hora_estado_fin='" + hora_estado_fin + '\'' +
                '}';
    }

    public Tracking(String nombre_estado, String descricion_estado, String comentario, String cod_operacion, String id_cliente, String fecha_estado_ini, String hora_estado_ini, String fecha_estado_fin, String hora_estado_fin) {
        this.nombre_estado = nombre_estado;
        this.descricion_estado = descricion_estado;
        this.comentario = comentario;
        this.cod_operacion = cod_operacion;
        this.id_cliente = id_cliente;
        this.fecha_estado_ini = fecha_estado_ini;
        this.hora_estado_ini = hora_estado_ini;
        this.fecha_estado_fin = fecha_estado_fin;
        this.hora_estado_fin = hora_estado_fin;
    }

    private String hora_estado_ini;
    private String fecha_estado_fin;
    private String hora_estado_fin;
}