package com.bbva.hackathon;

import com.bbva.hackathon.data.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
public class TrackingMongoController {
     private ArrayList<TrackingMongo> listaOrden = null;
    @Autowired
    private TrackingRepository repository;

    /* Get lista de cod_operacion */
    @GetMapping(value = "/cod_operacion", produces = "application/json")
    public ResponseEntity<List<TrackingMongo>> obtenerListado()
    {
        List<TrackingMongo> lista = repository.findAll();
        return new ResponseEntity<List<TrackingMongo>>(lista, HttpStatus.OK);
    }
    /* Get Cliente by operacion */
    @GetMapping(value = "/cod_operacionbyopera/O={operacion}", produces = "application/json")
    public ResponseEntity<List<TrackingMongo>> obtenerEstadoporOperacion(@PathVariable String operacion)
    {
        System.out.println("Consulta por nombre ");
        List<TrackingMongo> resultado = repository.findCod_operacion(operacion);
        return new ResponseEntity<List<TrackingMongo>>(resultado, HttpStatus.OK);
    }

/////////////////
/* Add nuevo operacion */
/////////////////
    @PostMapping(value="/cod_operacion")
    public ResponseEntity<String> addoperacion(@RequestBody TrackingMongo trackingMongo)
    {
        Date date = new Date();
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        System.out.println("Hora: " + hourFormat.format(date));
        System.out.println("Fecha: " + hourFormat.format(date));

        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
             System.out.println("ID: " + timestamp);
             System.out.println("ID: " + timestamp.getTime());

        trackingMongo.setId(timestamp.getTime());
        trackingMongo.setFecha_estado_ini(dateFormat.format(date));
        trackingMongo.setHora_estado_ini(hourFormat.format(date));

        TrackingMongo resultado = repository.insert(trackingMongo);
        return new ResponseEntity<String>(resultado.toString(), HttpStatus.OK);
    }

    @DeleteMapping(value="/cod_operacion/{operacion}")
    public ResponseEntity<String> deleteCliente(@PathVariable String operacion)
    {
        /* Posibilidad de buscar la operacion, después eliminarlo si existe */
        repository.deleteById(operacion);
        return new ResponseEntity<String>("operacion borrado", HttpStatus.OK);
    }

    @PutMapping(value="/cod_operacion/{cod_operacion}", produces = "application/json")
    public ResponseEntity<String> updateOperacion(@PathVariable String cod_operacion, @RequestBody TrackingMongo operacion)
    {
        System.out.println("entro");

        ResponseEntity<String> resultado = null;
        try {
            System.out.println("variable cod_operacion" + cod_operacion);
            List<TrackingMongo> operacionAModificar = this.repository.findCod_operacionEstado(cod_operacion, operacion.nombre_estado);
            System.out.println("Voy a modificar la fecha y hora del estado la Operacion");

            Date date = new Date();
            DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
            DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            System.out.println("Hora: " + hourFormat.format(date));
            System.out.println("Fecha: " + hourFormat.format(date));

            System.out.println(operacionAModificar.get(0));
            TrackingMongo indexmongo = operacionAModificar.get(0);
            indexmongo.setFecha_estado_fin(dateFormat.format(date));
            indexmongo.setHora_estado_fin(hourFormat.format(date));
            //     indexmongo.setFecha_estado_fin(operacion.getFecha_estado_fin());
            //    indexmongo.setHora_estado_fin(operacion.getHora_estado_fin());
            repository.delete(indexmongo);
            repository.save(indexmongo);

            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;

    }
}


